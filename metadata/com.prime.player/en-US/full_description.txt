Experience the ultimate Audio Player with our Music Player app - the perfect Media Player for all your favorite tunes! Our Mp3 Player supports all music and audio formats including MP3, MIDI, WAV, FLAC, AAC, APE, and more, ensuring you never miss a beat. Enjoy high-quality HD Dolby Atmos sound and explore your music library by Artists, Genres, and Albums. With our easy-to-use interface and seamless playback, our Media Player is the perfect choice for music lovers.

Featured
- Android Auto support
- Chromecast support
- Gapless playback support
- Multiple Now playing themes (more coming in the future updates)
- Fade in/Fade out music when pause/resume playback
- Normal and synchronized lyrics support
- Multiple artists support (Split artists with custom separators)
- Multiple genres support (Split genres with custom separators)
- Download and edit lyrics from the app itself
- Amoled theme
- Change accent color and highlight color
- Sleep timer
- Replay gain support
- Inbuilt equalizer (Upcoming).
- 3rd Party equalizer support.
- Custom playlist support (No more worrying about playlists getting deleted
automatically)
- Import and export playlists (Upcoming)
- Multiple sorting options
- Light, dark, battery saver and system default theme support
- Dedicated folders section(view your music files from the app itself)
- Delightful animations, animated icons
- Song tag editor, album tag editor (Upcoming)
- Follows latest material design guidelines
- Just 5 MB in size

Download now and enjoy the ultimate Music experience!
